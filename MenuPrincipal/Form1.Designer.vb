﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelCurrent = New System.Windows.Forms.Label()
        Me.TextBoxMax = New System.Windows.Forms.TextBox()
        Me.TextBoxMatricula = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxNombre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxGrade = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxUnidad = New System.Windows.Forms.TextBox()
        Me.ButtonClear = New System.Windows.Forms.Button()
        Me.ButtonSaveAndCont = New System.Windows.Forms.Button()
        Me.ButtonSaveBinary = New System.Windows.Forms.Button()
        Me.ButtonStoreLaps = New System.Windows.Forms.Button()
        Me.ButtonPromedios = New System.Windows.Forms.Button()
        Me.ButtonReadBinary = New System.Windows.Forms.Button()
        Me.ButtonExit = New System.Windows.Forms.Button()
        Me.ButtonShowProm = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LabelCurrent
        '
        Me.LabelCurrent.AutoSize = True
        Me.LabelCurrent.Location = New System.Drawing.Point(30, 28)
        Me.LabelCurrent.Name = "LabelCurrent"
        Me.LabelCurrent.Size = New System.Drawing.Size(39, 13)
        Me.LabelCurrent.TabIndex = 0
        Me.LabelCurrent.Text = "Label1"
        '
        'TextBoxMax
        '
        Me.TextBoxMax.Location = New System.Drawing.Point(75, 21)
        Me.TextBoxMax.Name = "TextBoxMax"
        Me.TextBoxMax.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxMax.TabIndex = 1
        '
        'TextBoxMatricula
        '
        Me.TextBoxMatricula.Location = New System.Drawing.Point(126, 83)
        Me.TextBoxMatricula.Name = "TextBoxMatricula"
        Me.TextBoxMatricula.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxMatricula.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(72, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Matricula"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(72, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nombre"
        '
        'TextBoxNombre
        '
        Me.TextBoxNombre.Location = New System.Drawing.Point(126, 109)
        Me.TextBoxNombre.Name = "TextBoxNombre"
        Me.TextBoxNombre.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxNombre.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(61, 164)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Calificación"
        '
        'TextBoxGrade
        '
        Me.TextBoxGrade.Location = New System.Drawing.Point(126, 161)
        Me.TextBoxGrade.Name = "TextBoxGrade"
        Me.TextBoxGrade.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxGrade.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(72, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Unidad"
        '
        'TextBoxUnidad
        '
        Me.TextBoxUnidad.Location = New System.Drawing.Point(126, 135)
        Me.TextBoxUnidad.Name = "TextBoxUnidad"
        Me.TextBoxUnidad.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxUnidad.TabIndex = 6
        '
        'ButtonClear
        '
        Me.ButtonClear.Location = New System.Drawing.Point(292, 21)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(101, 20)
        Me.ButtonClear.TabIndex = 10
        Me.ButtonClear.Text = "Limpiar"
        Me.ButtonClear.UseVisualStyleBackColor = True
        '
        'ButtonSaveAndCont
        '
        Me.ButtonSaveAndCont.Location = New System.Drawing.Point(269, 69)
        Me.ButtonSaveAndCont.Name = "ButtonSaveAndCont"
        Me.ButtonSaveAndCont.Size = New System.Drawing.Size(118, 40)
        Me.ButtonSaveAndCont.TabIndex = 11
        Me.ButtonSaveAndCont.Text = "Almacenar y Continuar"
        Me.ButtonSaveAndCont.UseVisualStyleBackColor = True
        '
        'ButtonSaveBinary
        '
        Me.ButtonSaveBinary.Enabled = False
        Me.ButtonSaveBinary.Location = New System.Drawing.Point(269, 112)
        Me.ButtonSaveBinary.Name = "ButtonSaveBinary"
        Me.ButtonSaveBinary.Size = New System.Drawing.Size(59, 40)
        Me.ButtonSaveBinary.TabIndex = 12
        Me.ButtonSaveBinary.Text = "Guardar Archivo"
        Me.ButtonSaveBinary.UseVisualStyleBackColor = True
        '
        'ButtonStoreLaps
        '
        Me.ButtonStoreLaps.Location = New System.Drawing.Point(179, 21)
        Me.ButtonStoreLaps.Name = "ButtonStoreLaps"
        Me.ButtonStoreLaps.Size = New System.Drawing.Size(47, 20)
        Me.ButtonStoreLaps.TabIndex = 13
        Me.ButtonStoreLaps.Text = "Set!"
        Me.ButtonStoreLaps.UseVisualStyleBackColor = True
        '
        'ButtonPromedios
        '
        Me.ButtonPromedios.Enabled = False
        Me.ButtonPromedios.Location = New System.Drawing.Point(269, 158)
        Me.ButtonPromedios.Name = "ButtonPromedios"
        Me.ButtonPromedios.Size = New System.Drawing.Size(118, 23)
        Me.ButtonPromedios.TabIndex = 14
        Me.ButtonPromedios.Text = "Promediar"
        Me.ButtonPromedios.UseVisualStyleBackColor = True
        '
        'ButtonReadBinary
        '
        Me.ButtonReadBinary.Location = New System.Drawing.Point(334, 112)
        Me.ButtonReadBinary.Name = "ButtonReadBinary"
        Me.ButtonReadBinary.Size = New System.Drawing.Size(53, 40)
        Me.ButtonReadBinary.TabIndex = 15
        Me.ButtonReadBinary.Text = "Leer Archivo"
        Me.ButtonReadBinary.UseVisualStyleBackColor = True
        '
        'ButtonExit
        '
        Me.ButtonExit.Location = New System.Drawing.Point(12, 216)
        Me.ButtonExit.Name = "ButtonExit"
        Me.ButtonExit.Size = New System.Drawing.Size(65, 27)
        Me.ButtonExit.TabIndex = 16
        Me.ButtonExit.Text = "Salir"
        Me.ButtonExit.UseVisualStyleBackColor = True
        '
        'ButtonShowProm
        '
        Me.ButtonShowProm.Location = New System.Drawing.Point(269, 187)
        Me.ButtonShowProm.Name = "ButtonShowProm"
        Me.ButtonShowProm.Size = New System.Drawing.Size(118, 23)
        Me.ButtonShowProm.TabIndex = 17
        Me.ButtonShowProm.Text = "Mostrar Promedios"
        Me.ButtonShowProm.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(458, 255)
        Me.Controls.Add(Me.ButtonShowProm)
        Me.Controls.Add(Me.ButtonExit)
        Me.Controls.Add(Me.ButtonReadBinary)
        Me.Controls.Add(Me.ButtonPromedios)
        Me.Controls.Add(Me.ButtonStoreLaps)
        Me.Controls.Add(Me.ButtonSaveBinary)
        Me.Controls.Add(Me.ButtonSaveAndCont)
        Me.Controls.Add(Me.ButtonClear)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxGrade)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TextBoxUnidad)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxMatricula)
        Me.Controls.Add(Me.TextBoxMax)
        Me.Controls.Add(Me.LabelCurrent)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelCurrent As Label
    Friend WithEvents TextBoxMax As TextBox
    Friend WithEvents TextBoxMatricula As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxNombre As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxGrade As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TextBoxUnidad As TextBox
    Friend WithEvents ButtonClear As Button
    Friend WithEvents ButtonSaveAndCont As Button
    Friend WithEvents ButtonSaveBinary As Button
    Friend WithEvents ButtonStoreLaps As Button
    Friend WithEvents ButtonPromedios As Button
    Friend WithEvents ButtonReadBinary As Button
    Friend WithEvents ButtonExit As Button
    Friend WithEvents ButtonShowProm As Button
End Class
