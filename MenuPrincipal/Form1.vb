﻿Imports System.IO


Public Class Form1

    Dim matriculas(1) As Int16
    Dim nombres(1) As String
    Dim unidades(1) As Int16
    Dim grades(1) As Decimal

    Dim current = 1
    Dim max = 1

    Dim promedioFinal As Decimal

    Dim libreria As New LibreriaPromedios.Class1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub ButtonSaveAndCont_Click(sender As Object, e As EventArgs) Handles ButtonSaveAndCont.Click

        ButtonStoreLaps.Enabled = False
        TextBoxMax.Enabled = False

        ' Almacena valores solo si no hemos llegamos al limite
        If (TextBoxMatricula.Text <> "") Then
            matriculas(current - 1) = CType(TextBoxMatricula.Text, Int16)
        Else matriculas(current - 1) = 0
        End If

        If (TextBoxNombre.Text <> "") Then
            nombres(current - 1) = CType(TextBoxNombre.Text, String)
        Else nombres(current - 1) = 0
        End If

        If (TextBoxUnidad.Text <> "") Then
            unidades(current - 1) = CType(TextBoxUnidad.Text, Int16)
        Else unidades(current - 1) = 0
        End If

        If (TextBoxGrade.Text <> "") Then
            grades(current - 1) = CType(TextBoxGrade.Text, Decimal)
        Else grades(current - 1) = 0
        End If
        current += 1
        TurnOff()
    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        TextBoxMatricula.Enabled = True
        TextBoxNombre.Enabled = True
        TextBoxUnidad.Enabled = True
        TextBoxGrade.Enabled = True

        ButtonSaveAndCont.Enabled = True
        ButtonSaveBinary.Enabled = False
        ButtonPromedios.Enabled = False
    End Sub

    Private Sub ButtonSaveBinary_Click(sender As Object, e As EventArgs) Handles ButtonSaveBinary.Click

        ' Creador de Archivo
        Dim binCreator As Stream = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\DatosUno.bin",
                                      FileMode.Create, 'FileMode.Append
                                      FileAccess.Write)

        ' Escritor de archivo
        Dim binWriter As New BinaryWriter(binCreator)
        Dim i As Int16
        For i = 1 To max Step 1
            binWriter.Write(matriculas(i - 1))
            binWriter.Write(nombres(i - 1))
            binWriter.Write(unidades(i - 1))
            binWriter.Write(grades(i - 1))
        Next
        binWriter.Close()

    End Sub

    Private Sub ButtonReadBinary_Click(sender As Object, e As EventArgs) Handles ButtonReadBinary.Click

        ' Localizador de binario
        Dim binSearch As Stream = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\DatosUno.bin",
                                      FileMode.Open,
                                      FileAccess.Read)
        Dim binReader As New BinaryReader(binSearch)
        Do Until (binReader.PeekChar = -1)

            TextBoxMatricula.Text = binReader.ReadInt16
            TextBoxNombre.Text = binReader.ReadString
            TextBoxUnidad.Text = binReader.ReadInt16
            TextBoxGrade.Text = binReader.ReadDecimal

            TextBoxMatricula.Refresh()
            TextBoxNombre.Refresh()
            TextBoxUnidad.Refresh()
            TextBoxGrade.Refresh()

            Threading.Thread.Sleep(1000)
        Loop
        binReader.Close()

    End Sub

    Private Sub ButtonExit_Click(sender As Object, e As EventArgs) Handles ButtonExit.Click
        Me.Close()
    End Sub

    Private Sub ButtonPromedios_Click(sender As Object, e As EventArgs) Handles ButtonPromedios.Click
        ' Llamada a libreria
        promedioFinal = libreria.Promediar(grades, max)

    End Sub

    Private Sub ButtonShowProm_Click(sender As Object, e As EventArgs) Handles ButtonShowProm.Click
        Dim secundaria As New Ventana_Secundaria.Form1

        secundaria.setValores(promedioFinal, libreria.aprobatorio(), libreria.excelencia())
        secundaria.Show()
        Me.Hide()
    End Sub

    Private Sub ButtonStoreLaps_Click(sender As Object, e As EventArgs) Handles ButtonStoreLaps.Click


        If (TextBoxMax.Text = "") Then
            max = 1
        ElseIf (CType(TextBoxMax.Text, Int16) > 0) Then
            max = CType(TextBoxMax.Text, Int16)
            ' Si la cantidad maxima de veces no es default y es primera vez; redimensionar arreglos
            ReDim matriculas(max)
            ReDim nombres(max)
            ReDim unidades(max)
            ReDim grades(max)
        End If
        TextBoxMax.Text = max
        TextBoxMax.Enabled = False
        LabelCurrent.Text = 0
        ButtonStoreLaps.Enabled = False

    End Sub

    Private Sub TurnOff()
        If (current > max) Then
            ' Ya llegamos al maximo definido, entonces modifica enabled values.
            TextBoxMatricula.Enabled = False
            TextBoxNombre.Enabled = False
            TextBoxUnidad.Enabled = False
            TextBoxGrade.Enabled = False

            ButtonSaveAndCont.Enabled = False
            ButtonSaveBinary.Enabled = True
            ButtonPromedios.Enabled = True
            ButtonShowProm.Enabled = True

        Else LabelCurrent.Text = current
        End If
    End Sub
End Class
