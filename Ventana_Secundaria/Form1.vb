﻿Public Class Form1

    Dim promedio As Int16
    Dim aproba As Int16
    Dim excel As Int16

    Private Sub ButtonReturn_Click(sender As Object, e As EventArgs) Handles ButtonReturn.Click
        Dim principal As New MenuPrincipal.Form1
        Me.Hide()
        principal.Show()
    End Sub

    Sub setValores(ByVal prom As Decimal,
                   ByVal aproba As Int16,
                   ByVal excel As Int16)
        promedio = prom
        Me.aproba = aproba
        Me.excel = excel
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LabelPromedio.Text = CType(promedio, String)

        LabelAproba.Text = CType(aproba, String)
        LabelExcel.Text = CType(excel, String)
    End Sub
End Class
