﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonReturn = New System.Windows.Forms.Button()
        Me.LabelPromedio = New System.Windows.Forms.Label()
        Me.LabelAproba = New System.Windows.Forms.Label()
        Me.LabelExcel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ButtonReturn
        '
        Me.ButtonReturn.Location = New System.Drawing.Point(94, 192)
        Me.ButtonReturn.Name = "ButtonReturn"
        Me.ButtonReturn.Size = New System.Drawing.Size(75, 23)
        Me.ButtonReturn.TabIndex = 0
        Me.ButtonReturn.Text = "Regresar"
        Me.ButtonReturn.UseVisualStyleBackColor = True
        '
        'LabelPromedio
        '
        Me.LabelPromedio.AutoSize = True
        Me.LabelPromedio.Location = New System.Drawing.Point(119, 77)
        Me.LabelPromedio.Name = "LabelPromedio"
        Me.LabelPromedio.Size = New System.Drawing.Size(13, 13)
        Me.LabelPromedio.TabIndex = 1
        Me.LabelPromedio.Text = "0"
        '
        'LabelAproba
        '
        Me.LabelAproba.AutoSize = True
        Me.LabelAproba.Location = New System.Drawing.Point(80, 124)
        Me.LabelAproba.Name = "LabelAproba"
        Me.LabelAproba.Size = New System.Drawing.Size(13, 13)
        Me.LabelAproba.TabIndex = 2
        Me.LabelAproba.Text = "0"
        '
        'LabelExcel
        '
        Me.LabelExcel.AutoSize = True
        Me.LabelExcel.Location = New System.Drawing.Point(186, 124)
        Me.LabelExcel.Name = "LabelExcel"
        Me.LabelExcel.Size = New System.Drawing.Size(13, 13)
        Me.LabelExcel.TabIndex = 3
        Me.LabelExcel.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Aprobatorio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(160, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Excelencia"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(91, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Prom Gral"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelExcel)
        Me.Controls.Add(Me.LabelAproba)
        Me.Controls.Add(Me.LabelPromedio)
        Me.Controls.Add(Me.ButtonReturn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonReturn As Button
    Friend WithEvents LabelPromedio As Label
    Friend WithEvents LabelAproba As Label
    Friend WithEvents LabelExcel As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
End Class
