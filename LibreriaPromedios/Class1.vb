﻿Public Class Class1

    Dim aprobado, excelente

    Public Function Promediar(ByRef grades,
                              ByVal size) As Int16


        Dim i As Int16
        Dim promedio As Int16
        For i = 0 To size Step 1
            promedio += grades(i)
            If grades(i) > 70 Then
                aprobado += 1
            End If
            If grades(i) = 100 Then
                excelente += 1
            End If
        Next
        promedio = (promedio / size)

        Return (promedio)
    End Function

    Public Function aprobatorio() As Int16
        Return (aprobado)
    End Function

    Public Function excelencia() As Int16
        Return (excelente)
    End Function
End Class
